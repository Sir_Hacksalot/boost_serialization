#include "thing.h"
#include <vector>
#include <fstream>
#include "FileManager.h"


int main()
{
	Thing myThing;

	FileManager::save(myThing, "file.bin");

	Thing newThing;
	newThing.description = "This will be interesting.\n";
	FileManager::restore(newThing, "file.bin");

	newThing.printVector();
	std::cout << newThing.description << std::endl;

	
	std::vector<std::string> items;

	items.push_back("Four");
	items.push_back("Five");
	items.push_back("Six");

	FileManager::saveVec(items, "stuff.bin");

	std::vector<std::string> savedItems;

	FileManager::restoreVec(savedItems, "stuff.bin");

	for (std::string str : savedItems)
	{
		std::cout << str << std::endl;
	}



	system("PAUSE");
	return 0;
}
