#include "thing.h"



Thing::Thing()
{
	init();
}



Thing::~Thing()
{
}

void Thing::printVector()
{
	for (Stuff s : myStuff)
	{
		std::cout << s.toString();
		s.printVector();
	}
}

void Thing::init()
{
	//fill the vector with stuff
	for (int i = 0; i < 3; i++)
	{
		Stuff stuff("Barry", i);
		myStuff.push_back(stuff);
	}
}
