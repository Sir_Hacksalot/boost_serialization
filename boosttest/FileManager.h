#pragma once
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <vector>
#include "thing.h"
#include <fstream>

class FileManager
{
public:
	~FileManager();

	static void save(const Thing &thing, const char* filename);
	static void restore(Thing &thing, const char * filename);
	static void saveVec(const std::vector<std::string> &stuffVec, const char* filename);
	static void restoreVec(std::vector<std::string> &stuffVec, const char * filename);

private:
	FileManager();	
};

