#pragma once
#include <boost\serialization\access.hpp>
#include <boost\serialization\vector.hpp>
#include <boost\serialization\string.hpp>

#include <iostream>
#include <string>

class Stuff
{
public:
	Stuff();
	Stuff(std::string name, int number);
	~Stuff();

	std::string toString();
	void printVector();

private:
	std::string name;
	int number;
	std::vector<std::string> text;

	void init();

	friend class boost::serialization::access;
	friend std::ostream &operator<<(std::ostream &os, const Stuff &s);

	template<class Archive>
	void serialize(Archive &ar, const unsigned int)
	{
		ar &name;
		ar &number;
		ar &text;
	}
};
