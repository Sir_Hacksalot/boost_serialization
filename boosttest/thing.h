#pragma once
#include <vector>
#include <boost\serialization\vector.hpp>
#include <boost\serialization\access.hpp>
#include "Stuff.h"

class Thing
{
public:
	Thing();
	~Thing();

	void printVector();
	std::string description;

private:
	void init();

	std::vector<Stuff> myStuff;


	friend class boost::serialization::access;

	template<typename Archive>
	void serialize(Archive& ar, const unsigned version)
	{
		ar & myStuff;
	}
};

