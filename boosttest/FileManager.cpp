#include "FileManager.h"



FileManager::FileManager()
{
}


FileManager::~FileManager()
{
}

void FileManager::save(const Thing & thing, const char * filename)
{
	// make an archive
	std::ofstream ofs(filename);
	boost::archive::binary_oarchive outputArchive(ofs);
	outputArchive << thing;
}

void FileManager::restore(Thing & thing, const char * filename)
{
	// open the archive
	std::ifstream ifs(filename);
	boost::archive::binary_iarchive inputArchive(ifs);

	// restore the schedule from the archive
	inputArchive >> thing;
}

void FileManager::saveVec(const std::vector<std::string> &stuffVec, const char * filename)
{
	// make an archive
	std::ofstream ofs(filename);
	boost::archive::binary_oarchive outputArchive(ofs);
	outputArchive << stuffVec;
}

void FileManager::restoreVec(std::vector<std::string> &stuffVec, const char * filename)
{
	// open the archive
	std::ifstream ifs(filename);
	boost::archive::binary_iarchive inputArchive(ifs);

	// restore the schedule from the archive
	inputArchive >> stuffVec;
}
