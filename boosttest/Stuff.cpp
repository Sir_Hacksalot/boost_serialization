#include "Stuff.h"

Stuff::Stuff()
{
	init();
}

Stuff::Stuff(std::string name, int number) : Stuff()
{
	this->name = name;
	this->number = number;
}

Stuff::~Stuff() {}

std::string Stuff::toString()
{
	return "Name: " + name + '\n' + "Number: " + std::to_string(number) + '\n';
}

void Stuff::init()
{
	text.push_back("One");
	text.push_back("Two");
	text.push_back("Three");
}

std::ostream & operator<<(std::ostream & os, const Stuff & s)
{
	return os << s.name << s.number;
}

void Stuff::printVector()
{
	for (std::string s : text)
	{
		std::cout << s << std::endl;
	}
}